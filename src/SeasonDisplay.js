import './SeasonDisplay.css';
import React from 'react';

const seasonConfig = {
    Verano: {
        text: "Un verano naranja",
        iconName: "massive sun icon"
    },
    Invierno: {
        text: "Esta helao juan",
        iconName: "massive snowflake icon"
    }
}

const getSeason = (lat, month) => {
    if (month > 2 && month < 9) {
        return lat > 0 ? 'Verano' : 'Invierno';
    } else {
        return lat > 0 ? 'Invierno' : 'Verano';
    }
}

const SeasonDisplay = (props) => {
    
    const {text, iconName} = seasonConfig[getSeason(props.lat, new Date().getMonth())];
    
    return (
        <div className={`season-display ${getSeason(props.lat, new Date().getMonth())}`}>
            <i className={`icon-left ${iconName}`}></i>
            <h1>{ text }</h1>
            <i className={`icon-right ${iconName}`}></i>
        </div>
    )
}

export default SeasonDisplay;